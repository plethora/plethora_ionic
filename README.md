<div style="text-align: center">
    <img src="logo.png" alt="plethora logo">
    <p>a nagios-like application aiming at the IOT world!</p>
</div>

<hr>

# | Plethora ionic

We decided to use ionic to build the app and the website. 
Currently most of the effort were focused on the app which is more polished.

On the front you will be able to see the metrics of your device in nice charts.

## Contributors

+ [Constant Deschietere](mailto:constant.deschietere@cpe.fr)
+ [Rudy Deal](mailto:rudy.deal@cpe.fr)
+ [Maxime Collot](mailto:maxime.collot@cpe.fr)
+ [Florian Croisot](mailto:constant.deschietere@cpe.fr)
+ [Jordan Quagliatini](mailto:jordan.quagliatini@cpe.fr)
