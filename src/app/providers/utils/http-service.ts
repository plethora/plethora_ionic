import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

//import { Observable } from 'rxjs/Observable';
import { Config } from '../../shared/config';

@Injectable()
export class HttpService {

  private headers: HttpHeaders;

  constructor(private _http: HttpClient) {
    this.headers = new HttpHeaders().set('content-Type', 'application/json');
  }

  public get<T>(route: string) :Promise<T>{
    return this._http.get<T>(Config.API_ENDPOINT + route, {headers: this.headers}).toPromise();
  }

  // private handleError(error) {
  //   console.error(error);
  //   return Observable.throw(error);
  // }

}
