import { Injectable } from '@angular/core';

import { Terminal } from '../models/terminal';
import { HttpService } from './utils/http-service';

@Injectable()
export class TerminalService {
	
	constructor(private http: HttpService) { }

	getTerminals(): Promise<Terminal[]>{
		return this.http.get<Terminal[]>('devices');
	}

	getTerminalModules(device: Terminal): Promise<Terminal>{
		return this.http.get<Terminal>(`devices/${device.device_id}`);
	}

	getModulesData(device: Terminal, moduleName: string): Promise<any>{
		return this.http.get<any>(`devices/${device.device_id}/${moduleName}`);
	}

	getModulesDataPlot(device: Terminal, moduleName: string): Promise<Array<string>>{
		let date = new Date();
		let path = `devices/${device.device_id}/${moduleName}/plot/time?from=` + (date.getTime() - (30*60*1000)) + "&to=" + date.getTime();
		return this.http.get<Array<any>>(path);
	}

	getModulesDataPlotNow(device: Terminal, moduleName: string): Promise<any>{
		return this.http.get<any>(`devices/${device.device_id}/${moduleName}/plot`)
	}
}