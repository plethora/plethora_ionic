import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { PageListTerminal } from './pages/page-list-terminal/page-list-terminal.component';
import { PageDetailTerminalModule } from './pages/page-detail-terminal/page-detail-terminal.component.module';
import { SharedModule } from './shared/shared.module';

import { HttpClientModule } from '@angular/common/http';
import { HttpService } from './providers/utils/http-service';
import { TerminalService } from './providers/terminal-service';


@NgModule({
  declarations: [
    MyApp,
    PageListTerminal
  ],
  imports: [
    BrowserModule,
    PageDetailTerminalModule,
    HttpClientModule,
    SharedModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    PageListTerminal
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HttpService,
    TerminalService,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
