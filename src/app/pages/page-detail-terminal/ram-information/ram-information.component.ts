import { Component, Input, OnInit } from '@angular/core';

import { Terminal } from '../../../models/terminal';

import { TerminalService } from '../../../providers/terminal-service';

@Component({
  selector: 'app-ram-information',
  templateUrl: './ram-information.component.html'
})
export class RamInformationComponent implements OnInit {

  @Input() terminal: Terminal;

  ramLabel = "RAM";
  ramInformation:string = "";

  constructor(private terminalService: TerminalService) { }

  ngOnInit() {
    this.terminalService.getModulesData(this.terminal, "RAM_HARDWARE").then(data => {
      if (data.data){
        this.ramInformation = (data.data/ 1024).toFixed(0) + "MB";
      }else{
        this.ramInformation = "No information";
      } 
    });
  }

  getValues() {
  	return this.terminalService.getModulesDataPlot(this.terminal, "RAM_LOAD");
  }

  getNow() {
  	return this.terminalService.getModulesDataPlotNow(this.terminal, "RAM_LOAD");
  }

}
