import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RamInformationComponent } from './ram-information.component';

describe('RamInformationComponent', () => {
  let component: RamInformationComponent;
  let fixture: ComponentFixture<RamInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RamInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RamInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
