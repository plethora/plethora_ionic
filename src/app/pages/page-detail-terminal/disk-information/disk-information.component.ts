import { Component, Input, OnInit } from '@angular/core';

import { Terminal } from '../../../models/terminal';

import { TerminalService } from '../../../providers/terminal-service';

@Component({
  selector: 'app-disk-information',
  templateUrl: './disk-information.component.html'
})
export class DiskInformationComponent implements OnInit {

  @Input() terminal: Terminal;

  diskLabel = "Disk";

  diskInformation:string = "";

  public chartLabel = ["Disk load", "Empty space"];

  constructor(private terminalService: TerminalService) { };

  ngOnInit() {
  	this.terminalService.getModulesData(this.terminal, "DISK_CAPACITY").then(data => {
  		if (data.data){
  			this.diskInformation = (data.data/ 1024).toFixed(0) + "MB";
  		}else{
  			this.diskInformation = "No information";
  		}  		
 	});
  }

  getNow() {
  	return this.terminalService.getModulesData(this.terminal, "DISK_LOAD");
  }

}
