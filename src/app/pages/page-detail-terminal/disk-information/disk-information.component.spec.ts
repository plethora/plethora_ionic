import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DiskInformationComponent } from './disk-information.component';

describe('DiskInformationComponent', () => {
  let component: DiskInformationComponent;
  let fixture: ComponentFixture<DiskInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DiskInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DiskInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
