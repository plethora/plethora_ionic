import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CpuInformationComponent } from './cpu-information.component';

describe('CpuInformationComponent', () => {
  let component: CpuInformationComponent;
  let fixture: ComponentFixture<CpuInformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CpuInformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CpuInformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
