import { Component, Input, OnInit } from '@angular/core';

import { Terminal } from '../../../models/terminal';

import { TerminalService } from '../../../providers/terminal-service';

@Component({
  selector: 'app-cpu-information',
  templateUrl: './cpu-information.component.html'
})
export class CpuInformationComponent implements OnInit {

	@Input() terminal: Terminal;

	cpuLabel:string = "CPU";

  cpuInformation:string = "";

  constructor(private terminalService: TerminalService) { 
  }

  ngOnInit() {
    this.terminalService.getModulesData(this.terminal, "CPU_HARDWARE").then(data => {
      if (data.data){
        this.cpuInformation = data.data;
      }else{
        this.cpuInformation = "No information";
      } 
    });
  }

  getValues() {
  	return this.terminalService.getModulesDataPlot(this.terminal, "CPU_LOAD");
  }

  getNow() {
  	return this.terminalService.getModulesDataPlotNow(this.terminal, "CPU_LOAD");
  }

}
