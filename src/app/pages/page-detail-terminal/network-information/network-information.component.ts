import { Component, OnInit, Input } from '@angular/core';

import { Terminal } from '../../../models/terminal';

import { TerminalService } from '../../../providers/terminal-service';

@Component({
  selector: 'app-network-information',
  templateUrl: './network-information.component.html'
})
export class NetworkInformationComponent implements OnInit {

  @Input() terminal: Terminal;

  addressLabel:string = "Address";
  addressInformation:string = "";

  statusLabel:string = "Online";
  statusInformation:boolean = false;

  downLoadTitle = "Download";
  upLoadTitle = "Upload";

  constructor(private terminalService: TerminalService) { }

  ngOnInit() {
    this.terminalService.getModulesData(this.terminal, "IP_COMMON").then(data => {
      if (data.data){
        this.addressInformation = data.data;
      }else{
        this.addressInformation = "No information";
      } 
    });
    this.terminalService.getModulesData(this.terminal, "DEFAULT").then(data => {
      if (data.data){
        this.statusInformation = (data.data === "UP");
      } 
    });
  }

  getDownLoadValues() {
  	return this.terminalService.getModulesDataPlot(this.terminal, "NET_DOWNLOAD");
  }

  getDownLoadNow() {
  	return this.terminalService.getModulesDataPlotNow(this.terminal, "NET_DOWNLOAD");
  }

  getUpLoadValues() {
  	return this.terminalService.getModulesDataPlot(this.terminal, "NET_UPLOAD");
  }

  getUpLoadNow() {
  	return this.terminalService.getModulesDataPlotNow(this.terminal, "NET_UPLOAD");
  }

}
