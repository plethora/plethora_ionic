import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, PopoverController } from 'ionic-angular';

import { Terminal } from '../../models/terminal';
import { TerminalService } from '../../providers/terminal-service';
import { SettingsDetailComponent } from './settings-detail/settings-detail.component';

@IonicPage({
	name: 'PageDetailTerminal',
	segment: '/'
})
@Component({
  selector: 'page-detail-terminal',
  templateUrl: 'page-detail-terminal.component.html'
})
export class PageDetailTerminal {
	addressLabel = "Address";
	statusLabel = "Online";
	terminal: Terminal;
	displayCpu: boolean = false;
	displaySettingCpu: boolean = false;
	displayRam: boolean = false;
	displaySettingRam: boolean = false;
	displayDisk: boolean = false;
	displaySettingDisk: boolean = false;
	displayNetwork: boolean = false;
	displaySettingNetwork: boolean = false;

  constructor(public navCtrl: NavController, navParams: NavParams, public terminalService: TerminalService, private popoverCtrl: PopoverController) {
  	this.terminal = navParams.get('terminal');
  	this.terminalService.getTerminalModules(this.terminal).then(terminal => {
  		this.terminal.modules = [];
  		this.displayDisk = (terminal.modules.indexOf("DISK_LOAD") != -1);
  		this.displaySettingDisk = (terminal.modules.indexOf("DISK_LOAD") != -1);
  		this.displayCpu = (terminal.modules.indexOf("CPU_LOAD") != -1);
  		this.displaySettingCpu = (terminal.modules.indexOf("CPU_LOAD") != -1);
  		this.displayRam = (terminal.modules.indexOf("RAM_LOAD") != -1);
  		this.displaySettingRam = (terminal.modules.indexOf("RAM_LOAD") != -1);
  		this.displayNetwork = ((terminal.modules.indexOf("NET_DOWNLOAD") != -1) && (terminal.modules.indexOf("NET_UPLOAD") != -1));
  		this.displaySettingNetwork = ((terminal.modules.indexOf("NET_DOWNLOAD") != -1) && (terminal.modules.indexOf("NET_UPLOAD") != -1));
  	})
  }

  public showSettings(ev) {
  	let popover = this.popoverCtrl.create(SettingsDetailComponent, {
      displayCpu: this.displayCpu,
      displaySettingCpu: this.displaySettingCpu,
      displayRam: this.displayRam,
      displaySettingRam: this.displaySettingRam,
      displayDisk: this.displayDisk,
      displaySettingDisk: this.displaySettingDisk,
      displayNetwork: this.displayNetwork,
      displaySettingNetwork: this.displaySettingNetwork
    });

    popover.present({
      ev: ev
    });
    popover.onDidDismiss(data => {
    	if (data != null){
	    	this.displayCpu = (data.cpu && this.displaySettingCpu );
	    	this.displayRam = (data.ram && this.displaySettingRam );
	    	this.displayDisk = (data.disk && this.displaySettingDisk );
	    	this.displayNetwork = (data.network && this.displaySettingNetwork );
	    }
    })
  }

}