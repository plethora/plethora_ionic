import { Component, OnInit } from '@angular/core';

import { NavParams, ViewController } from 'ionic-angular';

@Component({
  selector: 'app-settings-detail',
  templateUrl: './settings-detail.component.html'
})
export class SettingsDetailComponent implements OnInit {

  cpu: boolean;
  cpuAvailable: boolean;
  ram: boolean;
  ramAvailable: boolean;
  disk: boolean;
  diskAvailable: boolean;
  network: boolean;
  networkAvailable: boolean;

  constructor(private navParams: NavParams, public viewCtrl: ViewController) { }

  ngOnInit() {
  	if (this.navParams.data) {
  		this.cpu = this.navParams.data.displayCpu;
  		this.cpuAvailable = this.navParams.data.displaySettingCpu;
  		this.ram = this.navParams.data.displayRam;
  		this.ramAvailable = this.navParams.data.displaySettingRam;
  		this.disk = this.navParams.data.displayDisk;
  		this.diskAvailable = this.navParams.data.displaySettingDisk;
  		this.network = this.navParams.data.displayNetwork;
  		this.networkAvailable = this.navParams.data.displaySettingNetwork;
  	}
  }

  save() {
  	let data = { "cpu":this.cpu, "ram":this.ram, "disk":this.disk, "network":this.network };
  	this.viewCtrl.dismiss(data);
  }

}
