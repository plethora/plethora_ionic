import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { PageDetailTerminal } from './page-detail-terminal.component';
import { RamInformationComponent } from './ram-information/ram-information.component';
import { SharedModule } from '../../shared/shared.module';
import { CpuInformationComponent } from './cpu-information/cpu-information.component';
import { DiskInformationComponent } from './disk-information/disk-information.component';
import { NetworkInformationComponent } from './network-information/network-information.component';
import { SettingsDetailComponent } from './settings-detail/settings-detail.component';

@NgModule({
  declarations: [
    PageDetailTerminal,
    RamInformationComponent,
    CpuInformationComponent,
    DiskInformationComponent,
    NetworkInformationComponent,
    SettingsDetailComponent
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(PageDetailTerminal)
  ],
  exports: [
    PageDetailTerminal,
    RamInformationComponent,
    CpuInformationComponent,
    DiskInformationComponent,
    NetworkInformationComponent,
    SettingsDetailComponent
  ],
  entryComponents: [
    RamInformationComponent,
    CpuInformationComponent,
    DiskInformationComponent,
    NetworkInformationComponent,
    SettingsDetailComponent
  ]
})
export class PageDetailTerminalModule { }