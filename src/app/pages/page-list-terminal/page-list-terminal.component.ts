import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';

import { Terminal } from '../../models/terminal';
import { TerminalService } from '../../providers/terminal-service';

@Component({
  selector: 'page-list-terminal',
  templateUrl: 'page-list-terminal.component.html'
})
export class PageListTerminal implements OnInit{

	terminals: Terminal[];
	addressLabel = "Address";
	ramLabel = "RAM";
	cpuLabel = "CPU";
	
	constructor(public navCtrl: NavController, public terminalService: TerminalService) { 
	}

	ngOnInit(){		
		this.getTerminals(null);
	}

	openTerminal(terminal: Terminal){
		this.navCtrl.push('PageDetailTerminal', {terminal: terminal});
	}

	getTerminals(callback){
		this.terminalService.getTerminals().then(terminals => {
			terminals.forEach(terminal => {
				terminal.ram_hardware_formatted = (terminal.ram_hardware / 1024).toFixed(0) + "MB";
			});
			this.terminals = terminals;
			if (callback){
				callback();
			}
		});
	}

	refreshList(refresher){
		this.getTerminals(refresher.complete());
	}
}