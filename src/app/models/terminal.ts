export class Terminal{
	constructor(
		public device_id: number,
		public hostname: string,
		public ip_address: string,
		public status: boolean,
		public modules: Array<any>,
		public ram_hardware: number,
		public ram_hardware_formatted: string,
		public cpu_hardware: string,
		public disk_hardware: number,
		public disk_hardware_formatted: string
		) { }
}