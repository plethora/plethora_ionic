import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-simple-information',
  templateUrl: './simple-information.component.html'
})
export class SimpleInformationComponent {

  @Input() title: string;
  @Input() value: string;

  constructor() { }

}
