import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core';

import { Terminal } from '../../models/terminal';
import { TerminalService } from '../../providers/terminal-service';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-piechart',
  templateUrl: './piechart.component.html'
})
export class PiechartComponent implements OnInit, OnDestroy {

  @Input() terminal: Terminal;

  @Input() getNow: Function;

  @Input() labels: Array<string>;

  @ViewChild(BaseChartDirective) chart: BaseChartDirective;

  private intervalId: number;

  public chartLegend:boolean = true;

  public chartType:string = "doughnut";

  public chartColors:any = {
		backgroundColor: 'rgba(223,115,255,0.2)',
		borderColor: 'rgba(223,115,255,1)',
		pointBackgroundColor: 'rgba(223,115,255,1)',
		pointBorderColor: '#fff',
		pointHoverBackgroundColor: '#fff',
		pointHoverBorderColor: 'rgba(223,115,255,0.8)'
  }

  public data: number[] = [1,1];

  constructor(public terminalService: TerminalService) { }

  drawChart(value) {
  	this.data[0] = value;
  	this.data[1] = 100 - value;
  	this.chart.chart.update();
  }

  ngOnInit() {
  	this.getNow().then(value => {
  		this.drawChart(Number(value.data));
  	});
  	this.intervalId = setInterval(() => {
  		this.getNow().then(value => {
  			this.drawChart(Number(value.data));
  		});
  	}, 60000);
  }

  ngOnDestroy() {
  	clearInterval(this.intervalId);
  }

}
