export class Config {

	public static API_ENDPOINT = "https://plethora.cn-ds.fr/";

	public static MODULES = [
		"RAM_HARDWARE",
		"IP_ADDRESS",
		"DISK_CAPACITY",
		"CPU_CAPACITY",
		"CPU_HARDWARE"
	]
}