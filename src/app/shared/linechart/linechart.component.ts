import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core';

import { Terminal } from '../../models/terminal';
import { TerminalService } from '../../providers/terminal-service';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-linechart',
  templateUrl: './linechart.component.html'
})
export class LinechartComponent implements OnInit, OnDestroy {

	@Input() terminal: Terminal;

	@Input() getValues: Function;
	@Input() getNow: Function;

	@ViewChild(BaseChartDirective) chart: BaseChartDirective;

	private intervalId: number;

	public dataset:Array<any> = [
		{data : [{"x":"00.00", "y":"0"}]}
	];

	public labels: Array<any> = ["00.00"];

	public chartLegend:boolean = false;

	public chartType:string = "line";

	public chartOptions:any = {
	  responsive: true,
	  scales: {
		xAxes: [{
			type: "time",
			time: {
				parser: "HH:mm"
			},
			display: true,
			scaleLabel: {
				display: false,
				labelString: 'Time'
			}
		}],
		yAxes: [{
			display: true,
			scaleLabel: {
				display: true,
				labelString: 'Load %'
			},
			ticks: {
				min: 0,
				max: 100,
			}
		}]
	  }
  };

  private purple: string = '#441650';;
  private lightPurple: string = '#4416508e';

	public chartColors: any[] = [{
		backgroundColor: this.lightPurple,
		borderColor: this.purple,
		pointBackgroundColor: this.purple,
		pointBorderColor: '#fff',
		pointHoverBackgroundColor: '#fff',
		pointHoverBorderColor: 'rgba(223,115,255,0.8)'
	}]

	public displayChart = false;

  constructor(public terminalService: TerminalService) { }

  drawValues(values) {
  	if (values.length > 0){
  		this.displayChart = true;
	  	this.labels = values.map(value => value.x);
		this.dataset = Array.of({ data: values});
		this.chart.chart.config.data.labels = this.labels;
		this.chart.chart.update();
	}else{
		this.displayChart = false;
	}
  }

  ngOnInit() {
  	this.getValues().then(values => this.drawValues(values));
  	if (this.intervalId) clearInterval(this.intervalId);
  	this.intervalId = setInterval(() => {
  		this.getNow().then(value => {
  			const values = this.dataset[0].data.slice(1).concat(value);
  			this.drawValues(values);
  		});
  	}, 60000);
  }

  ngOnDestroy() {
  	clearInterval(this.intervalId);
  }
}
