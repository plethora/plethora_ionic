import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SimpleInformationComponent } from './simple-information/simple-information.component';
import { LinechartComponent } from './linechart/linechart.component';
import { ChartsModule } from 'ng2-charts';
import { PiechartComponent } from './piechart/piechart.component';
import { DataLinechartComponent } from './data-linechart/data-linechart.component';


@NgModule({
  imports: [
    CommonModule,
    ChartsModule
  ],
  declarations: [
    SimpleInformationComponent,
    LinechartComponent,
    PiechartComponent,
    DataLinechartComponent,
    ],
  exports: [
    SimpleInformationComponent,
    LinechartComponent,
    PiechartComponent,
    DataLinechartComponent,
  ],
  entryComponents: [
    SimpleInformationComponent,
    LinechartComponent,
    PiechartComponent,
    DataLinechartComponent
  ]
})
export class SharedModule {
}