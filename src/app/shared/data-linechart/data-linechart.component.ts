import { Component, OnInit, Input, ViewChild, OnDestroy } from '@angular/core';

import { Terminal } from '../../models/terminal';
import { TerminalService } from '../../providers/terminal-service';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'app-data-linechart',
  templateUrl: './data-linechart.component.html'
})
export class DataLinechartComponent implements OnInit, OnDestroy {
@Input() terminal: Terminal;

	@Input() getValues: Function;
	@Input() getNow: Function;
	@Input() title: string;

	@ViewChild(BaseChartDirective) chart: BaseChartDirective;

	private intervalId: number;

	public dataset:Array<any> = [
		{data : [{"x":"00.00", "y":"0"}]}	
	];

	public labels: Array<any> = ["00.00"];

	public chartLegend:boolean = false;

	public chartType:string = "line";

	public chartOptions:any = {
	  responsive: true,
	  scales: {
		xAxes: [{
			type: "time",
			time: {
				parser: "HH:mm"
			},
			display: true,
			scaleLabel: {
				display: false,
				labelString: 'Time'
			}
		}],
		yAxes: [{
			display: true,
			scaleLabel: {
				display: true,
				labelString: 'Load kB/s'
			}
		}]
	  }
	};

	public chartColors:any = {
		backgroundColor: 'rgba(223,115,255,0.2)',
		borderColor: 'rgba(223,115,255,1)',
		pointBackgroundColor: 'rgba(223,115,255,1)',
		pointBorderColor: '#fff',
		pointHoverBackgroundColor: '#fff',
		pointHoverBorderColor: 'rgba(223,115,255,0.8)'
	}

	public displayChart = false;

  constructor(public terminalService: TerminalService) { }

  drawValues(values) {
  	if (values.length > 0){
  		this.displayChart = true;
	  	this.labels = values.map(value => value.x);
		this.dataset = Array.of({ data: values});
		this.chart.chart.config.data.labels = this.labels;
		this.chart.chart.update();
	}else{
		this.displayChart = false;
	}
  }

  ngOnInit() {
  	console.log(this.title);
  	this.getValues().then(values => this.drawValues(values));
  	if (this.intervalId) clearInterval(this.intervalId);
  	this.intervalId = setInterval(() => {
  		this.getNow().then(value => {
  			const values = this.dataset[0].data.slice(1).concat(value);
  			this.drawValues(values);
  		});
  	}, 60000);
  }

  ngOnDestroy() {
  	clearInterval(this.intervalId);
  }
}

