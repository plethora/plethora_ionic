import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { PageListTerminal } from './pages/page-list-terminal/page-list-terminal.component';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = PageListTerminal;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

      var notificationOpenedCallback = function(jsonData) {
        console.log('notificationOpenedCallback: ' + JSON.stringify(jsonData));
      };
  
      if(window["plugins"] !== undefined) {
        window["plugins"].OneSignal
          .startInit("3d8235d7-306f-440d-8d86-b4ea9d0c554a", "103263069905")
          .handleNotificationOpened(notificationOpenedCallback)
          .endInit();
      }
    });
  }
}

